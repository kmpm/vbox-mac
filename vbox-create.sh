#!/bin/bash
set -e
VM=${1:-'macmini7-1'}

# VBoxManage list ostypes
OSID=MacOS1013_64
HWALT="./hw-alternatives/$VM.sh"
DEFAULT_MACHINE_FOLDER=`VBoxManage list systemproperties | grep "Default machine folder" | cut -b 34-`


if [[ ! -f $HWALT ]]; then
    echo 'First parameter must match name in hw-alternatives.'
    echo "'$HWALT' does not exist"
    exit 1
fi


if [ ! -d "$DEFAULT_MACHINE_FOLDER" ]; then
    DEFAULT_MACHINE_FOLDER="./"
fi

function debug {
    echo "VM='$VM'"
    echo "OSID='$OSID'"
    echo "DEFAULT_MACHINE_FOLDER=$DEFAULT_MACHINE_FOLDER"
    echo "MACHINE_FOLDER='$MACHINE_FOLDER'"
    echo "HWALT='$HWALT'"
    echo "CONFIG_FILE='$CONFIG_FILE'"
}


echo "Creating machine $VM"
VBoxManage createvm --name "$VM" --ostype "$OSID" --register


echo "Getting VM folder location for $VM"
CONFIG_FILE=`VBoxManage showvminfo "$VM" | grep "Config file" | cut -b 18-`
MACHINE_FOLDER=$(dirname "${CONFIG_FILE}")

if [ ! -d "$MACHINE_FOLDER" ]; then
    MACHINE_FOLDER=$DEFAULT_MACHINE_FOLDER
fi


echo "Creating virtual disk for $VM in '$MACHINE_FOLDER'"
VBoxManage createmedium disk --filename "$MACHINE_FOLDER/$VM.vdi" --size 32768


echo "Modifying $VM hardware options"
VBoxManage modifyvm "$VM" --boot1 disk --boot2 dvd --boot3 none --boot4 none 
VBoxManage modifyvm "$VM" --memory 4096 --cpus 2 --chipset ich9
VBoxManage modifyvm "$VM" --firmware efi64 --rtcuseutc on
VBoxManage modifyvm "$VM" --vram 128 --accelerate3d on
VBoxManage modifyvm "$VM" --usbxhci on
VBoxManage modifyvm "$VM" --mouse usbtablet --keyboard usb


echo "add SATA controller and some disk to $VM"
VBoxManage storagectl "$VM" --name "SATA Controller" --add sata --controller IntelAHCI
VBoxManage storageattach "$VM" --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "$VM.vdi"


echo "Adde empty DVD-Drive for installation media"
VBoxManage storageattach $VM --storagectl "SATA Controller" --port 1 \
    --device 0 --type dvddrive --medium emptydrive


echo "Modifying $VM with common options for Mac"
VBoxManage setextradata "$VM" "VBoxInternal/Devices/efi/0/Config/DmiSystemVersion" "1.0"
VBoxManage setextradata "$VM" "VBoxInternal/Devices/efi/0/Config/DmiBoardProduct" "Iloveapple"
VBoxManage setextradata "$VM" "VBoxInternal/Devices/smc/0/Config/DeviceKey" "ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc"
VBoxManage setextradata "$VM" "VBoxInternal/Devices/smc/0/Config/GetKeyFromRealSMC" 1


echo "Modifying $VM with hardware specific options for Mac"
source $HWALT


echo "Done creating $VM"
