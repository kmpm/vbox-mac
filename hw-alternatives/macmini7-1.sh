
# From a MacMini7,1 (late 2015) with 2.6 MHz CPU.

VBoxManage modifyvm "$VM" --cpuidset 00000001 000306a9 04100800 7fbae3ff bfebfbff
VBoxManage setextradata "$VM" "VBoxInternal/Devices/efi/0/Config/DmiSystemProduct" "Macmini7,1"
