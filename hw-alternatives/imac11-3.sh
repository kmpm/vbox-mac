
# https://forums.virtualbox.org/viewtopic.php?f=22&t=84164
# http://suzywu2014.github.io/ubuntu/2017/02/23/macos-sierra-virtualbox-vm-on-ubuntu

VBoxManage modifyvm "$VM" --cpuidset 00000001 000306a9 00020800 80000201 178bfbff
VBoxManage setextradata "$VM" "VBoxInternal/Devices/efi/0/Config/DmiSystemProduct" "iMac11,3"
