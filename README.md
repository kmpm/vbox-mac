Virtualbox with Mac Guest
=========================

This is tested and found working on 
* [x] Mac Mini (Late 2015) using macOS 10.13.5 (High Sierra) 


# Quick instructions
* Get 'High Sierra' from app store
* Install virtualbox + addons
* run `highsierra_bootable.sh` to generate ~/Desktop/HighSierra.iso
* run `vbox-create.sh` to generate a mac compatible VM
* add the iso to the cdrom drive using VirtualBox GUI
* Boot on iso
* Erase disk
* Install
* Eject ISO
* REBOOT

Due to something, something it will not work directly but you will get dumped into a uefi shell.
( https://www.virtualbox.org/ticket/17030 )
Please run the following to install properly. It will take as long if
not longer as the first install.

## Opt 1
```
Shell> fs1:
FS1:\> cd "macOS Install Data"
FS1:\macOS Install Data\> cd "Locked Files"
FS1:\macOS Install Data\Locked Files\> cd "Boot Files"
FS1:\macOS Install Data\Locked Files\Boot Files\> boot.efi
```

## Opt 2
* Reset VM
* Quickly press __Fn + F12__ to get into VirtualBox boot manager
* Select `Boot Maintenance Manager`
* Select `Boot from file`
* Select entry with `...HD(2, GPT)...`
* Select `<macOS Install Data>`
* Select `<Locked Files>`
* Select `<Boot Files>`
* Select `boot.efi`



## cpuid
* https://superuser.com/questions/625648/virtualbox-how-to-force-a-specific-cpu-to-the-guest

```bash
sysctl -n machdep.cpu.brand_string
# Intel(R) Core(TM) i5-4278U CPU @ 2.60GHz
vboxmanage list hostcpuids > cpu-alternatives/i5-4278U
```


# Vbox/Mac Stuff
* https://github.com/samm-git/gimp-osx-package/blob/master/README.md
* https://github.com/geerlingguy/macos-virtualbox-vm
* https://www.perkin.org.uk/posts/create-virtualbox-vm-from-the-command-line.html
* https://astr0baby.wordpress.com/2018/04/22/installing-high-sierra-10-13-4-in-virtualbox-5-x/
* https://forums.virtualbox.org/viewtopic.php?t=85631
* http://tobiwashere.de/2017/10/virtualbox-how-to-create-a-macos-high-sierra-vm-to-run-on-a-mac-host-system/