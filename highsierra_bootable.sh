#!/bin/bash
set -e
#en-US
APP_FILE="Install macOS High Sierra.app"


DMG_INSTALL_FILE="/Applications/$APP_FILE/Contents/SharedSupport/InstallESD.dmg"
BASE_DMG_FILE="/Applications/$APP_FILE/Contents/SharedSupport/BaseSystem.dmg"
BASE_CDR_FILE=/tmp/HighSierraBase.cdr

CDR_FILE=/tmp/HighSierra.cdr
OUTPUT=~/Desktop/HighSierra.iso

if [ -f $OUTPUT ]; then
    echo "$OUTPUT already exists. Remove manually before creating a new one"
    exit 1
fi

if [ ! -d "/Applications/$APP_FILE" ]; then
    echo "Use appstore to download '$APP_FILE'"
    ls "/Applications/$APP_FILE"
    exit 1
fi

if [ ! -f "$DMG_INSTALL_FILE" ]; then
    echo "Missing High Sierra InstallESD.dmg file"
    exit 1
fi

if [ ! -f "$BASE_DMG_FILE" ]; then
    echo "Missing High Sierra BaseSystem.dmg file"
    exit 1
fi


echo "Generate a BaseSystem.dmg with 10.13.3 Install Packages"
hdiutil attach "$DMG_INSTALL_FILE" -noverify -mountpoint /Volumes/highsierra

echo " - creating BASE_CDR_FILE"
hdiutil create -o "$BASE_CDR_FILE" -size 7316m -layout SPUD -fs HFS+J
hdiutil attach "$BASE_CDR_FILE.dmg" -noverify -mountpoint /Volumes/install_build

echo " - restoring BASE_DMG_FILE"
asr restore -source "$BASE_DMG_FILE" -target /Volumes/install_build -noprompt -noverify -erase
rm /Volumes/OS\ X\ Base\ System/System/Installation/Packages

echo " - copying files"
cp -R /Volumes/highsierra/Packages /Volumes/OS\ X\ Base\ System/System/Installation

echo " - cleaning up"
hdiutil detach /Volumes/OS\ X\ Base\ System/
hdiutil detach /Volumes/highsierra/
echo " - moving result to /tmp/BaseSystem.dmg"
mv "$BASE_CDR_FILE.dmg" /tmp/BaseSystem.dmg



echo "Restore the 10.13 Installer's BaseSystem.dmg into file system and place custom BaseSystem.dmg into the root"
echo " - creating CDR_FILE"
hdiutil create -o "$CDR_FILE" -size 8965m -layout SPUD -fs HFS+J
hdiutil attach "$CDR_FILE.dmg" -noverify -mountpoint /Volumes/install_build

echo " - restoring BASE_DMG_FILE"
asr restore -source "$BASE_DMG_FILE" -target /Volumes/install_build -noprompt -noverify -erase

echo " - copying"
cp /tmp/BaseSystem.dmg /Volumes/OS\ X\ Base\ System

echo " - cleaning up"
hdiutil detach /Volumes/OS\ X\ Base\ System/
rm /tmp/BaseSystem.dmg

echo " - converting to iso"
hdiutil convert "$CDR_FILE.dmg" -format UDTO -o /tmp/HighSierra.iso
mv /tmp/HighSierra.iso.cdr "$OUTPUT"

echo " - more cleanup"
rm "$CDR_FILE.dmg"

echo "Done with $OUTPUT"
